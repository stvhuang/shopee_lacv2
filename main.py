import argparse
import datetime
import json
import logging
import pathlib

import accelerate
import sklearn.metrics
import sklearn.model_selection
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm

from src.lacv2.datasets import LACDataset
from src.lacv2.models import LACModel

parser = argparse.ArgumentParser()
parser.add_argument("--batch_size", default=1024, type=int)
parser.add_argument("--exp", type=str)
parser.add_argument("--gradient_accumulation_steps", default=1, type=int)
parser.add_argument("--lr", default=1e-2, type=float)
parser.add_argument("--num_epochs", default=100, type=int)
parser.add_argument("--path_to_data_json", default="./data_test.json", type=pathlib.Path)
parser.add_argument("--path_to_finfo_json", default="./finfo_tset.json", type=pathlib.Path)
parser.add_argument("--weight_decay", default=1e-2, type=float)
args = parser.parse_args()

if args.exp is None:
    args.exp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")

save_dir = pathlib.Path(f"./save/{args.exp}")
save_dir.mkdir(parents=True, exist_ok=True)

logging.basicConfig(
    level=logging.INFO,
    handlers=[
        logging.FileHandler(save_dir / "log.txt"),
        logging.StreamHandler(),
    ],
)

for k, v in vars(args).items():
    logging.info(f"ARGS {k}: {v}")

with args.path_to_finfo_json.expanduser().open() as f:
    finfo = json.load(f)

with args.path_to_data_json.expanduser().open() as f:
    data = []

    for line in tqdm(f.read().splitlines()):
        data.append(json.loads(line))

data_train, data_valid = sklearn.model_selection.train_test_split(data, test_size=0.1)

model = LACModel(finfo=finfo)
optimizer = torch.optim.AdamW(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)

dataset_train = LACDataset(data_train)
dataloader_train = DataLoader(
    dataset_train, batch_size=args.batch_size, shuffle=True, collate_fn=dataset_train.collate_fn, drop_last=True
)

dataset_valid = LACDataset(data_valid)
dataloader_valid = DataLoader(
    dataset_valid, batch_size=args.batch_size, shuffle=False, collate_fn=dataset_valid.collate_fn, drop_last=False
)

criterion = torch.nn.BCEWithLogitsLoss()

accelerator = accelerate.Accelerator(gradient_accumulation_steps=args.gradient_accumulation_steps)
model = accelerator.prepare_model(model)
optimizer = accelerator.prepare_optimizer(optimizer)
dataloader_train = accelerator.prepare_data_loader(dataloader_train)
dataloader_valid = accelerator.prepare_data_loader(dataloader_valid)

for epoch in range(args.num_epochs):
    # train
    model.train(True)

    with tqdm(dataloader_train) as pbar:
        with accelerator.accumulate(model):
            for batch in pbar:
                x_list = [v for k, v in batch.items() if k != "y"]
                y_true = batch["y"].view(-1)

                # forward
                y_logs = model(*x_list).view(-1)
                loss = criterion(input=y_logs, target=y_true.float())

                # backward
                accelerator.backward(loss)

                # update
                optimizer.step()
                optimizer.zero_grad()

                pbar.set_description(f"epoch: {epoch:02d}, train, loss: {loss.item():2.4f}")
                pbar.update()

    # valid
    model.train(False)
    y_true_coll = []
    y_logs_coll = []

    with tqdm(dataloader_valid) as pbar:
        for batch in pbar:
            x_list = [v for k, v in batch.items() if k != "y"]
            y_true = batch["y"].view(-1)
            y_true_coll.extend(y_true.tolist())

            # forward
            with torch.no_grad():
                y_logs = model(*x_list).view(-1)

            y_logs_coll.extend(y_logs.tolist())

            # log
            pbar.set_description(f"epoch: {epoch:02d}, valid")
            pbar.update()

    # metrics
    y_prob_coll = torch.sigmoid(torch.tensor(y_logs_coll)).tolist()
    roc_auc = sklearn.metrics.roc_auc_score(y_true=y_true_coll, y_score=y_prob_coll)

    # log
    logging.info(f"epoch: {epoch:02d}, valid, roc_auc: {roc_auc:.4f}")

    # save
    torch_save_f = save_dir / "epoch{epoch:02d}__roc_auc{roc_auc:.4f}.pt"
    logging.info(f"saving model to {torch_save_f}")
    torch.save(model.state_dict(), torch_save_f)
    logging.info("saved model successfully")
