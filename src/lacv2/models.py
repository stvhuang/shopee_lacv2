import logging

import beartype
import jaxtyping
import numpy as np
import onnxruntime
import torch
from jaxtyping import Float, Int, Num
from torch import Tensor
from tqdm import trange
from transformers import AutoModel


class LACModel(torch.nn.Module):
    class LACFeatureEncoderDense(torch.nn.Module):
        def __init__(self, embedding_dim: int, dropout_p: float = 0.0) -> None:
            super().__init__()
            self.encoder = torch.nn.Sequential(
                torch.nn.Linear(in_features=1, out_features=embedding_dim),
                torch.nn.Dropout(p=dropout_p),
            )

        @jaxtyping.jaxtyped(typechecker=beartype.beartype)
        def forward(self, x: Float[Tensor, "B L"]) -> Float[Tensor, "B L D"]:
            return self.encoder(x.view(*x.shape, 1))

    class LACFeatureEncoderSparse(torch.nn.Module):
        def __init__(self, num_embeddings: int, embedding_dim: int, dropout_p: float = 0.0) -> None:
            super().__init__()
            self.encoder = torch.nn.Sequential(
                torch.nn.Embedding(num_embeddings=num_embeddings + 1, embedding_dim=embedding_dim),
                torch.nn.Dropout(p=dropout_p),
            )

        @jaxtyping.jaxtyped(typechecker=beartype.beartype)
        def forward(self, x: Int[Tensor, "B L"]) -> Float[Tensor, "B L D"]:
            return self.encoder(x + 1)

    class LACFeatureEncoderText(torch.nn.Module):
        def __init__(self, model_name_or_path: str = "microsoft/mdeberta-v3-base", dropout_p: float = 0.0) -> None:
            super().__init__()
            self.encoder = AutoModel.from_pretrained(model_name_or_path)

        @jaxtyping.jaxtyped(typechecker=beartype.beartype)
        def forward(
            self, input_ids: Int[Tensor, "B L"], attention_mask: Int[Tensor, "B L"] | None = None
        ) -> Float[Tensor, "B L D"]:
            return self.encoder(input_ids=input_ids, attention_mask=attention_mask).last_hidden_state

        @property
        def embedding_dim(self) -> int:
            return self.encoder.config.hidden_size

    class LACAvgPool(torch.nn.Module):
        def __init__(self, max_len: int) -> None:
            super().__init__()
            self.pool = torch.nn.AvgPool1d(kernel_size=max_len)

        @jaxtyping.jaxtyped(typechecker=beartype.beartype)
        def forward(self, out_encoder: Float[Tensor, "B L D"]) -> Float[Tensor, "B D"]:
            out_encoder_T: Float[Tensor, "B D L"] = out_encoder.transpose(1, 2)
            out_encoder_pooled: Float[Tensor, "B D 1"] = self.pool(out_encoder_T)
            out_encoder_pooled_squeezed: Float[Tensor, "B D"] = out_encoder_pooled.squeeze(-1)

            return out_encoder_pooled_squeezed

    class LACAttentionBasedPooling(torch.nn.Module):
        def __init__(self, embedding_dim: int) -> None:
            super().__init__()
            self.q_emb = torch.nn.Parameter(torch.randn(embedding_dim))

        @jaxtyping.jaxtyped(typechecker=beartype.beartype)
        def forward(self, k_embs: Float[Tensor, "B L D"]) -> Float[Tensor, "B D"]:
            attn_l: Float[Tensor, "B L"] = torch.einsum("bld,d->bl", k_embs, self.q_emb)
            attn_w: Float[Tensor, "B L"] = torch.nn.functional.softmax(attn_l, dim=-1)
            attn_o: Float[Tensor, "B D"] = torch.einsum("bl,bld->bd", attn_w, k_embs)

            return attn_o

    class LACFeedForward(torch.nn.Module):
        def __init__(
            self, inp_features: int, out_features: int, hid_features: int | None = None, dropout_p: float = 0.0
        ) -> None:
            super().__init__()
            hid_features = hid_features or out_features
            self.module = torch.nn.Sequential(
                torch.nn.Linear(in_features=inp_features, out_features=hid_features),
                torch.nn.ReLU(),
                torch.nn.Dropout(p=dropout_p),
                torch.nn.LayerNorm(normalized_shape=hid_features),
                torch.nn.Linear(in_features=hid_features, out_features=out_features),
            )

        @jaxtyping.jaxtyped(typechecker=beartype.beartype)
        def forward(self, embedding: Float[Tensor, "B ..."]) -> Float[Tensor, "B ..."]:
            return self.module(embedding)

    class LACMultiHeadAttention(torch.nn.Module):
        def __init__(self, embedding_dim: int, num_heads: int = 4) -> None:
            super().__init__()
            self.proj_q = torch.nn.Linear(in_features=embedding_dim, out_features=embedding_dim)
            self.proj_v = torch.nn.Linear(in_features=embedding_dim, out_features=embedding_dim)
            self.attn = torch.nn.MultiheadAttention(embed_dim=embedding_dim, num_heads=num_heads)

        @jaxtyping.jaxtyped(typechecker=beartype.beartype)
        def forward(self, x: Float[Tensor, "B L D"]) -> Float[Tensor, "B L D"]:
            q = self.proj_q(x)
            v = self.proj_v(x)
            attn_output, _ = self.attn(query=q, key=x, value=v)

            return attn_output

    @beartype.beartype
    def _init_encoder(self, finfo: dict, embedding_dim: int) -> torch.nn.Module:
        match finfo["type"]:
            case "dense":
                encoder_ = self.LACFeatureEncoderDense(embedding_dim=embedding_dim)
            case "sparse":
                # add 4 to the num_embeddings in case new indices are added
                encoder_ = self.LACFeatureEncoderSparse(
                    num_embeddings=finfo["max_idx"] + 4, embedding_dim=embedding_dim
                )
            case "text":
                encoder_ = self.LACFeatureEncoderText()
            case _:
                raise ValueError()

        encoder = torch.nn.Sequential(
            encoder_,
            self.LACAttentionBasedPooling(embedding_dim=embedding_dim),  # self.LACAvgPool(max_len=finfo["max_len"]),
            self.LACFeedForward(inp_features=embedding_dim, out_features=embedding_dim),
            torch.nn.ReLU(),
        )

        return encoder

    @beartype.beartype
    def __init__(self, finfo: dict, embedding_dim: int = 16) -> None:
        super().__init__()
        self.encoder_list = torch.nn.ModuleList(self._init_encoder(info, embedding_dim) for info in finfo.values())
        self.attn = self.LACMultiHeadAttention(embedding_dim=embedding_dim)
        self.attn_relu = torch.nn.ReLU()
        self.ff = self.LACFeedForward(
            inp_features=len(self.encoder_list) * embedding_dim, out_features=1, hid_features=32
        )

    @jaxtyping.jaxtyped(typechecker=beartype.beartype)
    def forward(self, *finput_list: Num[Tensor, "B ..."]) -> Float[Tensor, "B 1"]:
        # encode + pool + feed forward
        out_encoder_list = [encoder(finput) for encoder, finput in zip(self.encoder_list, finput_list)]
        out_encoder_list_stacked = torch.stack(out_encoder_list, dim=-2)  # B (F*D)
        # multi-head self-attention
        out_attn = self.attn(out_encoder_list_stacked)
        out_attn_reshaped = out_attn.view(out_attn.shape[0], -1)
        out_attn_reshaped_relu = self.attn_relu(out_attn_reshaped)
        # feed forward
        out_ffo = self.ff(out_attn_reshaped_relu)

        return out_ffo

    def to_onnx(self, finfo, export_f: str, export_verbose: bool = True) -> None:
        def generate_args(finfo):
            args = []

            for info in finfo.values():
                match info["type"]:
                    case "dense":
                        args.append(torch.randn((1, info["max_len"])))
                    case "sparse":
                        args.append(torch.randint(0, info["max_idx"], (1, info["max_len"])))
                    case "text":
                        raise NotImplementedError
                    case _:
                        raise ValueError()

            return args

        _training: bool = self.training
        self.train(False)

        args = generate_args(finfo)
        input_names = list(finfo.keys())
        output_names = ["output"]
        dynamic_axes = {name: {0: "batch_size"} for name in input_names + output_names}

        logging.info(f"Exporting model to ONNX: {export_f}")
        torch.onnx.export(
            model=self,
            args=tuple(args),
            f=export_f,
            verbose=export_verbose,
            input_names=input_names,
            output_names=output_names,
            opset_version=14,
            dynamic_axes=dynamic_axes,
        )

        # compare onnxruntime and pytorch outputs
        ort_session = onnxruntime.InferenceSession(export_f)

        logging.info("Comparing ORT and PyTorch outputs")

        for _ in trange(1_000):
            args = generate_args(finfo)

            # onnxruntime
            ort_inputs = {input_name: arg.numpy() for input_name, arg in zip(input_names, args)}
            ort_outputs = ort_session.run(None, ort_inputs)

            # torch
            with torch.no_grad():
                torch_outputs = self(*args)

            np.testing.assert_allclose(
                desired=ort_outputs[0],
                actual=torch_outputs.numpy(),
                rtol=1e-3,
                atol=1e-5,
                verbose=True,
            )

        logging.info("All outputs match!")

        self.train(_training)


__all__ = [
    "LACModel",
]


def __dir__():
    return __all__
