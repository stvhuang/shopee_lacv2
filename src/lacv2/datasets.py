import polars as pl
import torch
from torch.utils.data import Dataset


class LACDataset(Dataset):
    def __init__(
        self,
        df: pl.DataFrame,
    ) -> None:
        self.df = df.filter(
            pl.col("label__live_agent_offered") == 2,  # not offered
            pl.col("label__service_rating_id").is_in([1, 3]),
        ).select(
            (pl.col("label__service_rating_id") == 3).cast(pl.Int64).alias("y"),
            *[
                pl.col(name).alias(f"{name.replace('feature__', 'fid')}")
                for name in sorted(df.columns)
                if name.startswith("feature__")
            ],
        )

    def __len__(self) -> int:
        return len(self.df)

    def __getitem__(self, idx) -> dict[str, list]:
        return dict(zip(self.df.columns, [elem for elem in self.df.row(idx)]))

    @staticmethod
    def collate_fn(batch_LoD):
        batch_DoL = {key: torch.tensor([d[key] for d in batch_LoD]) for key in batch_LoD[0].keys()}

        return batch_DoL


__all__ = [
    "LACDataset",
]


def __dir__():
    return __all__
