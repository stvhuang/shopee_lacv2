import http.client
import json
import pathlib

import multiprocess as mp
from tqdm import tqdm


def load_config_dump_json(path_to_config_dump_json: str | pathlib.Path) -> dict:
    """https://chatbot-rcmdplt.shopee.io/fms/tools/sample -> Config Dump"""

    def process_basic_feature(feature: dict) -> dict:
        feature_processed = {
            "name": feature["feature_name"],
            "dtype": feature["value_type"],
            "primary_key": feature["primary_key"],
        }
        feature_processed["limit"] = None
        if feature_processed["dtype"].endswith("_LIST"):
            feature_processed["limit"] = int(feature["limit"])

        return feature_processed

    def process_derived_feature(feature: dict) -> dict:
        parameter_json = feature["bind_operator"]["parameter_json"]
        parameter_json = None if parameter_json == "" else json.loads(parameter_json)
        feature_processed = {
            "name": feature["feature_name"],
            "operator_class": feature["bind_operator"]["operator_class"],
            "basic_ids": sorted(feature["bind_operator"]["input"]),
            "parameter_json": parameter_json,
        }

        return feature_processed

    with pathlib.Path(path_to_config_dump_json).expanduser().open() as f:
        config_dump = json.load(f)

    feature_wrapper_list = config_dump["feature_wrapper_list"]
    feature_wrapper_list = [
        elem
        for elem in feature_wrapper_list
        if (
            elem.get("feature", {}).get("status", False) == "ENTITY_STATUS_ACTIVATED"
            or elem.get("derived_feature", {}).get("status", False) == "ENTITY_STATUS_ACTIVATED"
        )
    ]

    feature_processed_dict = {}

    for feature in feature_wrapper_list:
        if "feature" in feature:
            feature_processed = process_basic_feature(feature["feature"])
        elif "derived_feature" in feature:
            feature_processed = process_derived_feature(feature["derived_feature"])
        else:
            raise ValueError

        feature_processed["default_value"] = feature["default_value_config"]["default_value"]
        feature_processed_dict[feature["feature_id"]] = feature_processed

    feature_processed_dict = dict(sorted(feature_processed_dict.items(), key=lambda items: items[0]))
    feature_processed_dict = {
        key: dict(sorted(val.items(), key=lambda items: items[0])) for key, val in feature_processed_dict.items()
    }

    return feature_processed_dict


def load_feature_dict_json(path_to_feature_dict_json: str | pathlib.Path) -> dict:
    with pathlib.Path(path_to_feature_dict_json).expanduser().open() as f:
        feature_dict = json.load(f)

    for region in tqdm(feature_dict.keys()):
        for fid, mapping in feature_dict[region].items():
            if mapping:
                assert len(mapping) == max(mapping.values())

    feature_dict_fixed = {}

    for region in tqdm(feature_dict.keys()):
        # remove empty mappings
        dict_temp = {int(fid): mapping for fid, mapping in feature_dict[region].items() if mapping}

        # remove unused features
        dict_temp = {fid: mapping for fid, mapping in dict_temp.items() if fid not in {1029, 1030, 1053}}

        # reverse mapping from and to
        dict_temp = {
            fid: {mapping_to: mapping_from for mapping_from, mapping_to in mapping.items()}
            for fid, mapping in dict_temp.items()
        }

        # sort mappings
        dict_temp = {
            fid: dict(sorted(mapping.items(), key=lambda mapping_from_to: mapping_from_to[0]))
            for fid, mapping in dict_temp.items()
        }
        dict_temp = dict(sorted(dict_temp.items(), key=lambda fid_mapping: fid_mapping[0]))

        feature_dict_fixed[region] = dict_temp

    feature_dict_fixed = {region.upper(): val for region, val in feature_dict_fixed.items()}
    feature_dict_fixed = dict(sorted(feature_dict_fixed.items(), key=lambda items: items[0]))

    return feature_dict_fixed


def load_samples(path_to_sample_dir):
    def process_line(line):
        line = line.split("\x03")

        fdict = {}

        fdict["sample_id"] = line[0]
        fdict["show"] = line[1]
        fdict["click"] = line[2]

        assert line[3].startswith("#multitask")
        fdict["labels"] = line[3].split(":")[1::2]

        assert line[4] == "#item_id_list:"

        for fidfval in line[5:]:
            fid, fval = fidfval.split(":")
            fid = int(fid)

            if fid not in fdict:
                fdict[fid] = []

            fdict[fid].append(fval)

        fdict = dict(sorted(fdict.items(), key=lambda items: items[0]))

        return fdict

    data = []

    for txt in tqdm(sorted(pathlib.Path(path_to_sample_dir).glob("*.txt"))):
        with txt.open() as f:
            data += f.read().splitlines()

    pool = mp.Pool()
    fdict_list = pool.map(process_line, data)

    return fdict_list


def update_schema(
    author: str,
    schema_id: int,
    feature_ids: list[int],
    desc: str = "",
) -> http.client.HTTPResponse:
    conn = http.client.HTTPConnection("chatbot-rcmdplt.shopee.io")

    body = {
        "author": author,
        "schemas": [
            {
                "id": schema_id,
                "desc": desc,
                "feature_ids": sorted(feature_ids),
            }
        ],
    }
    body = json.dumps(body)

    conn.request(
        method="POST",
        url="/fms_api/schema/update?env=live",
        body=body,
        headers={"Content-Type": "application/json"},
    )

    response = conn.getresponse()
    response_body = response.read().decode()

    conn.close()

    print("Response Status:", response.status)
    print("Response Body:", response_body)

    return response


def sort_dict_by_key(d):
    return dict(sorted(d.items(), key=lambda items: items[0]))


def proc_fms_config_dump_json(path: str | pathlib.Path) -> dict[str, list[dict]]:
    path = pathlib.Path(path).expanduser()

    with path.open() as f:
        fms_config_dump = json.load(f)

    basic = {}
    derived_bucketing = {}
    derived_crossing = {}
    derived_mapping = {}

    for feat_dump in tqdm(fms_config_dump["feature_wrapper_list"]):
        id_ = feat_dump["feature_id"]
        info = {}

        if "feature" in feat_dump:
            feat = feat_dump["feature"]

            if feat["status"] != "ENTITY_STATUS_ACTIVATED":
                continue

            info["name"] = feat["feature_name"]
            info["dtype"] = feat["value_type"]
            info["primary_key"] = feat["primary_key"]
            info["datasource"] = feat["bind_datasource"]

            basic[id_] = info
        elif "derived_feature" in feat_dump:
            feat = feat_dump["derived_feature"]

            if feat["status"] != "ENTITY_STATUS_ACTIVATED":
                continue

            info["name"] = feat["feature_name"]
            info["operator"] = feat["bind_operator"]["operator_class"]
            info["parameter"] = json.loads(feat["bind_operator"]["parameter_json"] or "{}")

            match info["operator"]:
                case "BucketOperator":
                    info["basic_id"] = feat["bind_operator"]["input"][0]
                    derived_bucketing[id_] = info
                case "MappingOperator":
                    info["basic_id"] = feat["bind_operator"]["input"][0]
                    derived_mapping[id_] = info
                case "CrossHashOperator":
                    info["basic_id"] = feat["bind_operator"]["input"]
                    derived_crossing[id_] = info
                case "PreprocessPyOperator" | "TextTokenizerPyOperator" | "TokenizerOperator":
                    continue
                case _:
                    raise ValueError(f"{info['operator']=}")
        else:
            raise ValueError

    ####

    for id_, info in derived_bucketing.items():
        info.update(basic_info=basic[info["basic_id"]])
        basic_dtype = info["basic_info"]["dtype"]

    for id_, info in derived_mapping.items():
        info.update(basic_info=basic[info["basic_id"]])
        basic_dtype = info["basic_info"]["dtype"]

        match basic_dtype:
            case "FEATURE_VALUE_TYPE_INT32":
                model_dtype = ("i32", False)
            case "FEATURE_VALUE_TYPE_INT64":
                model_dtype = ("i64", False)
            case "FEATURE_VALUE_TYPE_STRING":
                model_dtype = ("str", False)
            case "FEATURE_VALUE_TYPE_INT32_LIST":
                model_dtype = ("i32", True)
            case "FEATURE_VALUE_TYPE_INT64_LIST":
                model_dtype = ("i64", True)
            case "FEATURE_VALUE_TYPE_STRING_LIST":
                model_dtype = ("str", True)
            case _:
                raise ValueError(f"{basic_dtype=}")

        info.update(model_dtype=model_dtype)

    ####

    basic = sort_dict_by_key(basic)
    derived_bucketing = sort_dict_by_key(derived_bucketing)
    derived_mapping = sort_dict_by_key(derived_mapping)

    return {
        "basic": basic,
        "derived_bucketing": derived_bucketing,
        "derived_crossing": derived_crossing,
        "derived_mapping": derived_mapping,
    }


__all__ = [
    "load_config_dump_json",
    "load_feature_dict_json",
    "load_samples",
    "update_schema",
    "proc_fms_config_dump_json",
]


def __dir__():
    return __all__
