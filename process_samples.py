import argparse
import datetime
import pathlib

import boto3
import multiprocess as mp
import polars as pl
import simplejson as json

parser = argparse.ArgumentParser()
parser.add_argument("--regions", nargs="+", type=str, required=True)
parser.add_argument("--date_beg", type=str, required=True)
parser.add_argument("--date_end", type=str, required=True)
# parser.add_argument("--path_fms_config_dump_json", type=str, required=True)
parser.add_argument("--path_fids_txt", type=pathlib.Path, required=True)
parser.add_argument("--path_samples", type=pathlib.Path, required=True)
args = parser.parse_args()


s3 = boto3.client(
    service_name="s3",
    endpoint_url="https://proxy.uss.s3.test.shopee.io",
    aws_access_key_id="60019773",
    aws_secret_access_key="KaBqEDyEJGguDXzJRiCoErDnBlWfEqII",
)

with args.path_fids_txt.open() as f:
    fids = [int(v) for v in f.read().splitlines()]

with (args.path_samples / "feature_dict.json").open("r") as f:
    feature_dict = json.load(f)


def parse_sample_line(line):
    data = {}

    line = line.split("\x03")

    data["session_id"] = line[0]

    idx = 1

    # labels
    while not line[idx].startswith("#label__"):
        idx += 1

    labels = {}

    while line[idx].startswith("#label__"):
        key, val = line[idx].split(":")
        key = key.replace("#label__", "label__")

        if key == "label__intent_id_array":
            val = [int(v) for v in val.split(",") if v not in {"", "null"}]
        else:
            val = int(val)

        labels[key] = val

        idx += 1

    data |= labels

    # features
    features = {}

    for feat in line[idx:]:

        id_, val = feat.split(":")

        if id_ == "#item_id_list":
            continue

        id_ = int(id_)

        if id_ not in fids:
            continue

        if id_ not in features:
            features[id_] = []

        features[id_].append(int(val))

    data |= {f"feature__{k:04d}": v for k, v in features.items()}

    return data


for region in args.regions:
    region = region.upper()

    sample_files = list(args.path_samples.glob("**/*.txt"))
    sample_files = [k for k in sample_files if k.parents[2].stem == region]
    sample_files = sorted(k for k in sample_files if args.date_beg <= k.parents[1].stem <= args.date_end)

    schema = None
    dfs = []

    for i, file in enumerate(sample_files):
        print(f"({i + 1}/{len(sample_files)}) {file}")

        with file.open() as f:
            samples = f.read().splitlines()

        if len(samples) == 0:
            print("empty!")
            continue

        p = mp.Pool()
        data = p.map_async(parse_sample_line, samples).get()

        if schema is None:
            schema_label = {k: pl.Int16 for k, v in data[0].items() if k.startswith("label__")}
            schema_label["label__intent_id_array"] = pl.List(pl.Int16)
            schema_feature = {k: pl.Array(pl.Int16, len(v)) for k, v in data[0].items() if k.startswith("feature__")}
            schema = {
                "session_id": pl.String,
                **schema_label,
                **schema_feature,
            }

        dfs.append(pl.DataFrame(data, schema=schema))
        del data

    df = pl.concat(dfs)

    filepath = f"processed_samples_{region}__{args.date_beg}_{args.date_end}__{datetime.datetime.now().strftime('%Y%m%d%H%M%S')}.parquet"

    df.write_parquet(filepath)
    print(f"saved to {filepath}")

    s3.upload_file(Bucket="live_agent_control", Key=f"datasets/{filepath}", Filename=filepath)
    print(f"uploaded to S3:live_agent_control/datasets/{filepath}")

s3.close()
