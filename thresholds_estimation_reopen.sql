WITH model_score AS (
  SELECT
    grass_region,
    grass_date,
    handler_id,
    pipeline_id,
    session_id,
    is_same_score,
    MAX(score) AS score
  FROM (
    SELECT
      region AS grass_region,
      DATE(dt) AS grass_date,
      CASE scene
      WHEN 'live_agent_control_landing_page' THEN
        'SessionLiveAgentControlHandler'
      WHEN 'live_agent_control_reopen' THEN
        'DialogueLiveAgentControlHandler'
      END AS handler_id,
      ('model_' || CASE scene
        WHEN 'live_agent_control_landing_page' THEN
          json_extract_scalar (data, '$.landing_page.model_name')
        WHEN 'live_agent_control_reopen' THEN
          split (json_extract_scalar (data, '$.reopen.model_name'), '_')[1]
        END) AS pipeline_id,
      sessionid AS session_id,
      CASE scene
      WHEN 'live_agent_control_landing_page' THEN
        1
      WHEN 'live_agent_control_reopen' THEN
        0
      END AS is_same_score,
      CASE scene
      WHEN 'live_agent_control_landing_page' THEN
        CAST(json_extract_scalar (data, '$.landing_page.score') AS DOUBLE)
      WHEN 'live_agent_control_reopen' THEN
        1.0 - CAST(json_extract_scalar (data, '$.reopen.score') AS DOUBLE)
      END AS score
    FROM
      chatbot_data.chatbot_liveagentcontrol_result__reg_continuous_s0_live
    WHERE
      from_unixtime (CAST(_timestamp AS BIGINT)) BETWEEN (timestamp '${BIZ_TIME}' - interval '168' hour)
      AND (timestamp '${BIZ_TIME}'))
  WHERE
    score IS NOT NULL
    AND pipeline_id IS NOT NULL
  GROUP BY
    grass_region,
    grass_date,
    handler_id,
    pipeline_id,
    session_id,
    is_same_score
)
SELECT
  map_agg (landing_page_show_percentage, data) AS data,
  primary_key,
  DATE(parse_datetime ('${PRE_DAY}', 'yyyyMMdd')) AS grass_date
FROM (
  SELECT
    IF (is_same_score = 0,
      map (transform (SEQUENCE (100, 0, -1), x -> x / 100.00), transform (values_at_quantiles (qdigest_agg (score), transform (SEQUENCE (0, 100, 1), x -> x / 100.00)), x -> 1 - x)),
      map (transform (SEQUENCE (0, 100, 1), x -> x / 100.00), values_at_quantiles (qdigest_agg (score), transform (SEQUENCE (0, 100, 1), x -> x / 100.00)))) AS data,
    landing_page_show_percentage,
    LOWER(CONCAT(grass_region, '_', landing_page_pipeline_id, '_', pipeline_id)) AS primary_key
  FROM (
  SELECT
    landing_page.grass_region,
    landing_page.grass_date,
    landing_page.session_id,
    landing_page_thresholds.show_percentage AS landing_page_show_percentage,
    landing_page.pipeline_id AS landing_page_pipeline_id,
    reopen.pipeline_id AS pipeline_id,
    reopen.is_same_score AS is_same_score,
    reopen.score AS score
  FROM (
  SELECT
    grass_region,
    grass_date,
    handler_id,
    pipeline_id,
    session_id,
    score
  FROM
    model_score
  WHERE
    handler_id = 'SessionLiveAgentControlHandler' AND grass_region NOT IN ('TW')) landing_page
  JOIN (
  -- landing page percentiles
  SELECT
    grass_region,
    pipeline_id,
    show_percentage,
    threshold
  FROM (
  SELECT
    grass_region,
    pipeline_id,
    map (transform (SEQUENCE (0, 100, 1), x -> x / 100.00), values_at_quantiles (qdigest_agg (score), transform (SEQUENCE (0, 100, 1), x -> x / 100.00))) AS data
  FROM
    model_score
  WHERE
    handler_id = 'SessionLiveAgentControlHandler' AND grass_region NOT IN ('TW')
  GROUP BY
    grass_region,
    pipeline_id)
  CROSS JOIN UNNEST(data) AS t (show_percentage,
  threshold)) landing_page_thresholds ON landing_page.grass_region = landing_page_thresholds.grass_region AND landing_page.pipeline_id = landing_page_thresholds.pipeline_id AND landing_page.score > landing_page_thresholds.threshold
JOIN (
SELECT
  *
FROM
  model_score
WHERE
  handler_id = 'DialogueLiveAgentControlHandler' AND grass_region NOT IN ('TW')) reopen ON landing_page.grass_region = reopen.grass_region AND landing_page.grass_date = reopen.grass_date AND landing_page.session_id = reopen.session_id)
GROUP BY
  grass_region,
  landing_page_pipeline_id,
  pipeline_id,
  landing_page_show_percentage,
  is_same_score)
GROUP BY
  primary_key
UNION
SELECT
  IF (is_same_score = 0,
    map (ARRAY[0.04], ARRAY[map (transform (SEQUENCE (100, 0, -1), x -> x / 100.00), transform (values_at_quantiles (qdigest_agg (score), transform (SEQUENCE (0, 100, 1), x -> x / 100.00)), x -> 1 - x))]),
    map (ARRAY[0.04], ARRAY[map (transform (SEQUENCE (0, 100, 1), x -> x / 100.00), values_at_quantiles (qdigest_agg (score), transform (SEQUENCE (0, 100, 1), x -> x / 100.00)))])) AS data,
LOWER(CONCAT(grass_region, '_', 'model_a', '_', pipeline_id)) AS primary_key,
DATE(parse_datetime ('${PRE_DAY}', 'yyyyMMdd')) AS grass_date
FROM
  model_score
WHERE
  handler_id = 'DialogueLiveAgentControlHandler' AND grass_region = 'TW'
GROUP BY
  grass_region,
  pipeline_id,
  is_same_score;

