import argparse
import json
import logging
import pathlib

import pandas as pd

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument("--path_to_dataset_json", "-p", type=pathlib.Path, required=True)
args = parser.parse_args()

logging.info(f"loading data from {args.path_to_dataset_json}")

data = []

with args.path_to_dataset_json.expanduser().open() as f:
    for line in f.read().splitlines():
        data.append(json.loads(line))

df = pd.DataFrame(data)

logging.info("remove columns fid18, fid1029, fid1030, fid1046, fid1047, fid1053")

df = df.loc[
    :,
    [
        col
        for col in df.columns
        if col
        not in {
            "fid18",
            "fid1029",
            "fid1030",
            "fid1046",
            "fid1047",
            "fid1053",
        }
    ],
]

logging.info("rename columns")

rename_columns = {c: f"x{int(c.replace('fid', '')):04d}" for c in df.columns if c != "labels"} | {"labels": "y"}
df = df.rename(columns=rename_columns)
df = df.loc[:, sorted(df.columns)]

df["y"] = df["y"].astype(int)

logging.info("create finfo")

finfo = {}

for xid, val in df.iloc[0].to_dict().items():
    if xid == "y":
        continue
    if isinstance(val[0], int):
        type_ = "sparse"
    elif isinstance(val[0], float):
        type_ = "dense"
    else:
        raise Exception(f"{val[0]=}, {type(val[0])=}")

    max_len = len(val)

    finfo[xid] = {
        "type": type_,
        "max_len": max_len,
    }

logging.info("fix max_idx for sparse features")

for fid, info in finfo.items():
    if info["type"] == "sparse":
        df_raw_col = df.loc[:, fid]

        finfo[fid]["max_idx"] = -1

        for vals in df_raw_col:
            finfo[fid]["max_idx"] = max(finfo[fid]["max_idx"], *vals)

        finfo[fid]["max_idx"] = finfo[fid]["max_idx"]

logging.info("save finfo.json")

with open(f"./{args.path_to_dataset_json.stem}_finfo.json", "w") as f:
    f.write(json.dumps(finfo, indent=2))

logging.info("save data.json")

with open(f"./{args.path_to_dataset_json.stem}_data.json", "w") as f:
    for _, row in df.iterrows():
        f.write(json.dumps(row.to_dict()) + "\n")
